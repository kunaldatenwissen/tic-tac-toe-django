from django.db import models
from django.db.models.enums import Choices

# Create your models here.


class Room(models.Model):
    room_name=models.CharField(max_length=50,unique=True)
    player1_name=models.CharField(max_length=50,default='Player 1')
    player2_name=models.CharField(max_length=50,default='Player 2')
    last_played=models.IntegerField(default=2)



class Game(models.Model):
    row_choices=[
        ('X','X'),
        ('O','O')
    ]
    row00=models.CharField(max_length=1,choices=row_choices,blank=True,null=True)
    row01=models.CharField(max_length=1,choices=row_choices,blank=True,null=True)
    row02=models.CharField(max_length=1,choices=row_choices,blank=True,null=True)
    row10=models.CharField(max_length=1,choices=row_choices,blank=True,null=True)
    row11=models.CharField(max_length=1,choices=row_choices,blank=True,null=True)
    row12=models.CharField(max_length=1,choices=row_choices,blank=True,null=True)
    row20=models.CharField(max_length=1,choices=row_choices,blank=True,null=True)
    row21=models.CharField(max_length=1,choices=row_choices,blank=True,null=True)
    row22=models.CharField(max_length=1,choices=row_choices,blank=True,null=True)
    room=models.ForeignKey(Room,on_delete=models.CASCADE)

