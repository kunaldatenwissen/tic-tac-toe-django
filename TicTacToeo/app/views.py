from django.http.response import JsonResponse
from django.shortcuts import redirect, render
from .models import *
from django.views.decorators.csrf import csrf_exempt
# Create your views here.

room=Room.objects.all()
# player=[2]

def index_page(request):

    if request.method == 'POST':

        room_name=request.POST.get('room_name')
        player1=request.POST.get('player1')
        player2=request.POST.get('player2')

        if room_name and player1 and player2:
            Room(room_name=room_name,player1_name=player1,player2_name=player2).save()

    context={
        'rooms':list(room.values())
    }

    return render(request,'index.html',context)



def room_page(request,pk):
    print(pk)
    

    room_data=room.get(id=pk)
    context={
        'room_data':room_data,
    }
    return render(request,'room.html',context)



@csrf_exempt
def check_roomname(request):
    data=''
    if request.method == 'POST':
        check=request.POST.get('check')
        print(check)
        print(room)
        for i in room:
            print(i.room_name)
            if i.room_name == check:
                data='Room name already taken'

    print(data)
    return JsonResponse({'data':data})


@csrf_exempt
def game(request,pk ,room_id):
    print(pk)
    print(room_id)
    room_instance=room.get(id=room_id)
    game_row=Game.objects.get(room=room_instance)
    if pk == 0:
        if room_instance.last_played == 2:
            print('player1 played in 00')
            game_row.row00 = 'X'
            game_row.save()
        if room_instance.last_played == 1:
            game_row.row00 = 'O'
            game_row.save()
    if pk == 1:
        if room_instance.last_played == 2:
           game_row.row01 = 'X'
           game_row.save()
        if room_instance.last_played == 1:
            game_row.row01 = 'O'
            game_row.save()
    if pk == 2:
        if room_instance.last_played == 2:
           game_row.row02 = 'X'
           game_row.save()
        if room_instance.last_played == 1:
            game_row.row02 = 'O'
            game_row.save()
    if pk == 10:
        if room_instance.last_played == 2:
           game_row.row10 = 'X'
           game_row.save()
        if room_instance.last_played == 1:
            game_row.row10 = 'O'
            game_row.save()
    if pk == 11:
        if room_instance.last_played == 2:
           game_row.row11 = 'X'
           game_row.save()
        if room_instance.last_played == 1:
            game_row.row11 = 'O'
            game_row.save()
    if pk == 12:
        if room_instance.last_played == 2:
           game_row.row12 = 'X'
           game_row.save()
        if room_instance.last_played == 1:
            game_row.row12 = 'O'
            game_row.save()
    if pk == 20:
        if room_instance.last_played == 2:
           game_row.row20 = 'X'
           game_row.save()
        if room_instance.last_played == 1:
            game_row.row20 = 'O'
            game_row.save()
    if pk == 21:
        if room_instance.last_played == 2:
           game_row.row21 = 'X'
           game_row.save()
        if room_instance.last_played == 1:
            game_row.row21 = 'O'
            game_row.save()
    if pk == 22:
        if room_instance.last_played == 2:
           game_row.row22 = 'X'
           game_row.save()
        if room_instance.last_played == 1:
            game_row.row22 = 'O'
            game_row.save()
        

    if room_instance.last_played == 2:
        room_instance.last_played = 1
        room_instance.save()
    elif room_instance.last_played == 1:
        room_instance.last_played = 2
        room_instance.save()

    return JsonResponse({'data':room_instance.last_played})



def delete_room(request,pk):


    room_delete=room.get(id=pk)
    room_delete.delete()  

    return JsonResponse({'data':'successfull'}) 


def check_who_won(request,pk):
    data=''
    room_instance=room.get(id=pk)
    send_game=Game.objects.filter(room=room_instance).values()
    check_game=Game.objects.get(room=room_instance)
    if check_game.row00 and check_game.row01 and check_game.row02  is not None:
        if check_game.row00 == check_game.row01 == check_game.row02:
            if check_game.row00 == 'X':
                data=''+room_instance.player1_name+' Won '
            if check_game.row00 == 'O':
                data=''+room_instance.player2_name+' Won '
    if check_game.row10 and check_game.row11 and check_game.row12  is not None:
        if check_game.row10 == check_game.row11 == check_game.row12:
            if check_game.row10 == 'X':
                data=''+room_instance.player1_name+' Won '
            if check_game.row10 == 'O':
                data=''+room_instance.player2_name+' Won '
    if check_game.row10 and check_game.row11 and check_game.row12  is not None:
        if check_game.row10 == check_game.row11 == check_game.row12:
            if check_game.row10 == 'X':
                data=''+room_instance.player1_name+' Won '
            if check_game.row10 == 'O':
                data=''+room_instance.player2_name+' Won '
    if check_game.row20 and check_game.row21 and check_game.row22  is not None:
        if check_game.row20 == check_game.row21 == check_game.row22:
            if check_game.row20 == 'X':
                data=''+room_instance.player1_name+' Won '
            if check_game.row20 == 'O':
                data=''+room_instance.player2_name+' Won '
    if check_game.row00 and check_game.row10 and check_game.row20  is not None:
        if check_game.row00 == check_game.row10 == check_game.row20:
            if check_game.row00 == 'X':
                data=''+room_instance.player1_name+' Won '
            if check_game.row00 == 'O':
                data=''+room_instance.player2_name+' Won '
    if check_game.row01 and check_game.row11 and check_game.row21  is not None:
        if check_game.row01 == check_game.row11 == check_game.row21:
            if check_game.row01 == 'X':
                data=''+room_instance.player1_name+' Won '
            if check_game.row01 == 'O':
                data=''+room_instance.player2_name+' Won '
    if check_game.row02 and check_game.row12 and check_game.row22  is not None:
        if check_game.row02 == check_game.row12 == check_game.row22:
            if check_game.row02 == 'X':
                data=''+room_instance.player1_name+' Won '
            if check_game.row02 == 'O':
                data=''+room_instance.player2_name+' Won '
    if check_game.row02 and check_game.row11 and check_game.row20  is not None:
        if check_game.row02 == check_game.row11 == check_game.row20:
            if check_game.row02 == 'X':
                data=''+room_instance.player1_name+' Won '
            if check_game.row02 == 'O':
                data=''+room_instance.player2_name+' Won '
    if check_game.row00 and check_game.row11 and check_game.row22  is not None:
        if check_game.row00 == check_game.row11 == check_game.row22:
            if check_game.row00 == 'X':
                data=''+room_instance.player1_name+' Won '
            if check_game.row00 == 'O':
                data=''+room_instance.player2_name+' Won '

    if check_game.row00 and check_game.row01 and check_game.row02 and check_game.row10 and check_game.row11 and check_game.row12 and check_game.row20 and check_game.row21 and check_game.row22 is not None:
        print('not none true')
        if check_game.row00 and check_game.row01 and check_game.row02 and check_game.row10 and check_game.row11 and check_game.row12 and check_game.row20 and check_game.row21 and check_game.row22 != '':
            print('not an empty string true')
            data='it was a draw to play again please reset'
    print(data)
    print(list(send_game))
    context={
        'game_data':list(send_game),
        'data':data,
        'player':room_instance.last_played,
    }

    return JsonResponse(context)



def reset_func(request,pk):
    room_instance=room.get(id=pk)
    game_reset=Game.objects.get(room=room_instance)
    game_reset.row00=''
    game_reset.row01=''
    game_reset.row02=''
    game_reset.row10=''
    game_reset.row11=''
    game_reset.row12=''
    game_reset.row20=''
    game_reset.row21=''
    game_reset.row22=''
    game_reset.save()
    room_instance.last_played=2
    room_instance.save()
    return JsonResponse({'data':'success'})