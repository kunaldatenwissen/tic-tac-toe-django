
$(document).ready(function () {

    var room_id = $('.room_id').attr('id')

    $(document).on('click', '.delete_row', function () {
        $.ajax({
            url: `/delete_room/${room_id}/`,
            type: 'GET',
            success: function (data) {
                // window.location('index_page')
                // location.href=''
                window.location = "/"
            }
        })
    })
   
    $(document).on('click', '.reset', function () {


        $.ajax({
            url: `/reset_func/${room_id}`,
            type: 'get',
            success: function (data) {
                console.log(data.data)
                $('.row00').val('')
                $('.row00').removeAttr('disabled')
                $('.row01').val('')
                $('.row01').removeAttr('disabled')
                $('.row02').val('')
                $('.row02').removeAttr('disabled')
                $('.row10').val('')
                $('.row10').removeAttr('disabled')
                $('.row11').val('')
                $('.row11').removeAttr('disabled')
                $('.row12').val('')
                $('.row12').removeAttr('disabled')
                $('.row20').val('')
                $('.row20').removeAttr('disabled')
                $('.row21').val('')
                $('.row21').removeAttr('disabled')
                $('.row22').val('')
                $('.row22').removeAttr('disabled')
                $('.player1').css('color', 'red')
                $('.player2').removeAttr('style')
            }
        })


    })





})