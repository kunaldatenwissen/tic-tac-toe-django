from django.db.models.signals import post_save, pre_save,post_delete,pre_delete

from django.dispatch import receiver

from .models import *




@receiver(post_save,sender=Room)
def create_game(sender,instance,created,**kwargs):
    if created:
        print('---------',instance)
        Game.objects.create(room=instance)