from django.urls import path
from .views import *



urlpatterns = [
    path('',index_page,name="index_page"),
    path('check_roomname/',check_roomname,name="check_roomname"),
    path('game/<int:pk>/<int:room_id>',game,name="game"),
    path('room_page/<int:pk>/',room_page,name="room_page"),
    path('check_who_won/<int:pk>/',check_who_won,name="check_who_won"),
    path('reset_func/<int:pk>/',reset_func,name="reset_func"),
    path('delete_room/<int:pk>/',delete_room,name="delete_room"),
]
